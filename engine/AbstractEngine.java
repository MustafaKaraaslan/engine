package at.kara.engine;

public abstract class AbstractEngine implements Engine {
	private int Serial;
	
	public AbstractEngine(int Serial){
		this.Serial = Serial;
	}

	public int getSerial() {
		return Serial;
	}

	public void setSerial(int serial) {
		Serial = serial;
	}

	
}
